
import Image from "next/image";
import Link from "next/link";

export const Header = ({ name, pages }) => {
    return (
        <header>
            <div>
                <nav>
                    {pages.map((page) => {
                        return (
                            <Link
                                aria-label={page.label}
                                key={page.id}
                                href={page.link}
                            >
                                {page.label}
                            </Link>
                        )
                    })}
                </nav>
                <h1>{name}</h1>
            </div>
            <Image
                priority
                width={2000}
                height={400}
                className=""
                src="/images/head_1.jpg"
                alt={`Image of ${name}`}
                aria-label={`Image of ${name}`}
            />
        </header>
    );
};
